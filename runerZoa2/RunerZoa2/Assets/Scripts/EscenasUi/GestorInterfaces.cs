﻿using UnityEngine;
using System.Collections;

public class GestorInterfaces : MonoBehaviour {

	public GameObject[] interfaces; 
	// Use this for initialization
	void Start () {
		interfaces[0].SetActive(true); 
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void  habilitarInterfaz(int num){

		desabilitarInterfaz();
		interfaces[num].SetActive(true);
	}

	public void desabilitarInterfaz(){
		foreach(GameObject ui in interfaces)
		{
			ui.SetActive(false);
		}

	}
}
