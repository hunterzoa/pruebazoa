﻿using UnityEngine;
using System.Collections;

public class RecargarEscenas : MonoBehaviour {
	int numeroNivel;
	public GameObject[] interfaces; 
	// Use this for initialization
	void Start () {
		NotificationCenter.DefaultCenter().AddObserver(this,"activarGuiPerdio");
		NotificationCenter.DefaultCenter().AddObserver(this,"activarGuiGano");
		numeroNivel=GestorJuego.gestor.ultimoMapaJuegado;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void recargarNivel(){
		switch(numeroNivel){
		case 1: Application.LoadLevel("runerivil1");
			break;
		case 2: Application.LoadLevel("runerivil2");
			break;
		case 3: Application.LoadLevel("runerivil3");
			break;
		case 4: Application.LoadLevel("runerivil4");
			break;
		default: Application.LoadLevel("runerivil1");
			break;
			
		}
		Application.LoadLevel("runerivil1");
	}

	public void volverMenuPrincipal(){

		Application.LoadLevel("menuPrincipal");
	}

	public void admiInterfacesNivel(int num){

		desabilitarInterfaz();
		interfaces[num].SetActive(true);
	}

	public void desabilitarInterfaz(){

		foreach(GameObject ui in interfaces)
		{
			ui.SetActive(false);
		}
		
	}

	public void activarGuiPerdio(){
		admiInterfacesNivel(0);

	}
	public void activarGuiGano(){
		admiInterfacesNivel(1);
		Camera.main.GetComponent<DatosPuntajes>().almacenarResultados();
		//GestorJuego.gestor.guardar();
		
	}
}
