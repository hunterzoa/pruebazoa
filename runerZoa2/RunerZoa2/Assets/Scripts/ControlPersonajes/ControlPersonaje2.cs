﻿using UnityEngine;
using System.Collections;

public class ControlPersonaje2 : MonoBehaviour {
	
	float capacidadSalto=215f;
	bool tocandoSuelo;
	public Transform comprobanteTocarSuelo;
	float comprobandoDistanciaSuelo=0.05f;
	public LayerMask mascaraSuelo;
	public bool corriendo=false;
	bool dobleSalto=false;
	public float velocidad=0.005f;
	public bool parar=false;
	public Rigidbody2D rb;
	// Use this for initialization
	void Start () {
		rb=GetComponent<Rigidbody2D>();
		
	}
	void FixedUpdate(){
		if(corriendo ){
		
			rb.velocity=new Vector2(velocidad+1,rb.velocity.y);

			}
			
		tocandoSuelo=Physics2D.OverlapCircle(comprobanteTocarSuelo.position,comprobandoDistanciaSuelo,mascaraSuelo);
		if(tocandoSuelo){
			dobleSalto=false;
		}
		//print("se esta tocando el suelo ????"+tocandoSuelo);
	}	


	void Update () {
		
		if(Input.GetMouseButtonDown(0)){
			
			if(corriendo){
				if( (tocandoSuelo || !dobleSalto)){
					if(!tocandoSuelo){
						personaje1(true);
					//	rb.velocity=new Vector2(0,3.2f);
					//	dobleSalto=true;
						
					}else{
						personaje1(false);
						//rb.velocity=new Vector2(0,4.8f);
						
					}
				}
				
			}else{
				corriendo=true;
			}
		}
		
	}


	public void personaje1(bool factor){
		if(factor){
			rb.velocity=new Vector2(0,3.2f);
			dobleSalto=true;
		}else{
			rb.velocity=new Vector2(0,4.8f);
		}
		
	}
	void OnTriggerStay2D(Collider2D other) {
		
		print(other.name);
		
	}
	
	
}
