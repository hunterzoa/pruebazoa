﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DatosPuntajes : MonoBehaviour {
	protected int numeroNivel=1;
	int puntuacionNivel;
	int oroNivel;
	int estrellas;
	int score;
	public Text textoOro;
	public Text textoScore;
	// Use this for initialization
	void Start () {
		NotificationCenter.DefaultCenter().AddObserver(this,"incrementarPuntos");
		NotificationCenter.DefaultCenter().AddObserver(this,"incrementarOro");
		NotificationCenter.DefaultCenter().AddObserver(this,"incrementarScore");
		score=0;
		oroNivel=0;

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void incrementarPuntos(Notification	notificacion){

		int puntosAicrementar=(int)notificacion.data;
		puntuacionNivel+=puntosAicrementar;
	
		//print("se dieron punticos ->"+puntuacionNivel);;
	}

	void incrementarOro(Notification	notificacion){
		
		int oroAicrementar=(int)notificacion.data;
		oroNivel+=oroAicrementar;
		print("se dieron monedas de oro ->"+oroNivel);;
	}

	void incrementarScore(Notification	notificacion){
		
		int scoreAicrementar=(int)notificacion.data;
		score+=scoreAicrementar;
		print("el score es ->"+score);;
	}

	public void almacenarResultados(){
	
		switch(numeroNivel){
		case 1:
			if(GestorJuego.gestor.score1<score){
				GestorJuego.gestor.score1=score;
			}
			break;
		case 2:
			if(GestorJuego.gestor.score2<score){
				GestorJuego.gestor.score2=score;
			}
			break;
		case 3:
			if(GestorJuego.gestor.score3<score){
				GestorJuego.gestor.score3=score;
			}
			break;
		case 4:
			if(GestorJuego.gestor.score4<score){
				GestorJuego.gestor.score4=score;
			}
			break;
		}
		print("puntuacion antes de actualizarce->"+GestorJuego.gestor.puntuaccion);
		GestorJuego.gestor.oro+=oroNivel;
		GestorJuego.gestor.puntuaccion+=puntuacionNivel;
	
		GestorJuego.gestor.guardar();
		print("puntuacion DESPUES de actualizarce->"+score.ToString() +"---"+GestorJuego.gestor.puntuaccion);
		textoOro.text=oroNivel.ToString();
		textoScore.text=score.ToString();
	}
}
