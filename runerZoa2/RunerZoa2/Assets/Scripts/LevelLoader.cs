﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {

	public GameObject loadingScene;
	public Image loadingBar;
	public string name;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setName(string name){
		this.name = name;
	}

	public void LoadLevel(){
		loadingScene.SetActive (true);
		StartCoroutine (LevelCoroutine());
	}

	IEnumerator LevelCoroutine(){

		AsyncOperation asyc = Application.LoadLevelAsync (name);
		while(!asyc.isDone && asyc.progress!=1f){

			loadingBar.fillAmount=asyc.progress;
			yield return null;
		}
	}
}
