﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GestorJuego : MonoBehaviour {

	public static GestorJuego gestor;

	public GameObject[] personajes;
	public GameObject[] logros;
	public GameObject personajeActual;
	int ultimaSeleccion;
	protected string rutaArchivo;


	public  int puntuaccion;
	public  int oro;
	public  int score1;
	public  int score2;
	public  int score3;
	public  int score4;

	public int estdoLogro1;
	public int ultimoMapaJuegado;


	// Use this for initialization
	void Awake(){
		rutaArchivo=Application.persistentDataPath+"/datos2.dat";

		if(gestor==null){
			gestor=this;
			DontDestroyOnLoad(gameObject);
		}else if(gestor!=this){
			Destroy(gameObject);

		} 
		personajeActual=personajes[0]; 

	}
	void Start () {
		cargar();
		seleccionPersonaje(ultimaSeleccion);
		achievements();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void seleccionPersonaje(int num){
		

		switch(num){
		case 1:
			personajeActual=personajes[0]; 
			break;
		case 2:
			personajeActual=personajes[1]; 
			break;
		case 3:
			personajeActual=personajes[2]; 
			break;
		case 4:
			personajeActual=personajes[3]; 
			break;
		default :
			personajeActual=personajes[0]; 
			break;
		}
		ultimaSeleccion=num;

		guardar();
	}
	public void achievements(){
		if(puntuaccion>=10){
			logros[0].GetComponent<Logros>().LogroCompletado();
			print("se a desbloqueado un logro");
		}

	}

	public void guardar(){
		
		BinaryFormatter bf=new BinaryFormatter();
		FileStream 	file=File.Create(rutaArchivo);
		
		DatosGuardar datos=new DatosGuardar();
		datos.ultimaSeleccionDT=ultimaSeleccion;
		datos.puntuaccionDT=puntuaccion;
		datos.oroDT=oro;
		datos.score1DT=score1;
		datos.score2DT=score2;
		datos.score3DT=score3;
		datos.score4DT=score4;
		datos.ultimoMapaJuegadoDT=ultimoMapaJuegado;


			
		bf.Serialize(file,datos );
		//print(datos.ultimaSeleccionDT);
		file.Close();
		//print(ultimaSeleccion+"*****"+personajeActual.name);
	}

	public void guardarPuntajes(){
		BinaryFormatter bf=new BinaryFormatter();
		FileStream 	file=File.Create(rutaArchivo);
		
		DatosGuardar datos=new DatosGuardar();
		datos.puntuaccionDT=puntuaccion;
		datos.oroDT=oro;
		datos.score1DT=score1;
		
		bf.Serialize(file,datos );
		file.Close();
	}

	void cargar(){
	
		if(File.Exists(rutaArchivo)){
			BinaryFormatter bf=new BinaryFormatter();
			FileStream 	file= File.Open(rutaArchivo,FileMode.Open);
			print("se esta cargando  datos");
			DatosGuardar datos=(DatosGuardar)bf.Deserialize(file);
			ultimaSeleccion=datos.ultimaSeleccionDT;
			puntuaccion=datos.puntuaccionDT;
			oro=datos.oroDT;
			ultimoMapaJuegado=datos.ultimoMapaJuegadoDT;
			score1=datos.score1DT;
			score2=datos.score2DT;
			score3=datos.score3DT;
			score4=datos.score4DT;
			file.Close();

		}else{
			ultimaSeleccion=1;
			puntuaccion=0;
			oro=0;
			score1=0;
			score2=0;
			score3=0;
			score4=0;
			ultimoMapaJuegado=1;
		}

	}

}


[Serializable]
class DatosGuardar{
	public int ultimaSeleccionDT;
	public int puntuaccionDT;
	public int oroDT;
	public int score1DT;
	public int score2DT;
	public int score3DT;
	public int score4DT;
	public int ultimoMapaJuegadoDT;
}
